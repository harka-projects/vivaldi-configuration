;(() => {
  const hover = (e, tab) => {
    const parent = tab.parentNode
    if (
      !parent.classList.contains('active') &&
      !e.shiftKey &&
      !e.ctrlKey &&
      !e.metaKey
    ) {
      tab.addEventListener('mouseleave', onMouseLeave)
      chrome.tabs.update(Number(parent.id.replace(/^\D+/, '')), {
        active: true,
        highlighted: true,
      })
    }
  }
  const onMouseLeave = e => {
    e.target.removeEventListener('mouseleave', onMouseLeave)
  }
  const appendChild = Element.prototype.appendChild
  Element.prototype.appendChild = function (newTab) {
    if (newTab.tagName === 'DIV' && newTab.classList.contains('tab')) {
      newTab.addEventListener('mouseenter', e => hover(e, newTab))
    }
    return appendChild.call(this, newTab)
  }
})()
