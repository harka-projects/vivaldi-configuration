#!/bin/bash

# Find the latest version directory
target=$(find "/Applications/Vivaldi.app/Contents/Frameworks/Vivaldi Framework.framework/Versions" -maxdepth 1 -type d -name '[0-9]*' | sort -Vr | head -n 1)/Resources/vivaldi

# Check if the target directory is found
if [[ -z $target ]]; then 
    echo "Failed to locate Vivaldi version folder."
    exit 1
fi

# Copy the vivaldi.css and vivaldi.js files to the target directory
cp ~/code/vivaldi-configuration/vivaldi.css "$target"
cp ~/code/vivaldi-configuration/vivaldi.js "$target"

# Read the content of vivaldi.txt
text=$(<~/code/vivaldi-configuration/vivaldi.txt)

# Escape special characters in the text
escaped_text=$(sed 's/[\/&]/\\&/g' <<< "$text")

# Insert the escaped text into the 9th line of browser.html
# Note: The syntax for sed on macOS requires an empty string '' after -i for in-place editing
sed -i '' "8s|^|${escaped_text//$'\n'/\\n}\\n|" "$target/window.html"

# Print success message
echo "Success!"
